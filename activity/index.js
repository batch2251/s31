let http = require('http'); 

http.createServer(function (req, res) {   
   
    if (req.url == '/login') {
            res.writeHead(200, { 'Content-Type': 'text/plain' });  
            res.end("Welcome to the login page.");  
    }
    else if (req.url == '/register') {
        
        res.writeHead(404, { 'Content-Type': 'text/plain' });  
        res.end("I'm sorry the page you are looking for cannot be found.");;
    
    }

}).listen(3000);

console.log('Node.js web server at port 3000 is running..')